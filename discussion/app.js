// console.log('Hello')

//JSON Objects
// JSON stands for javascript Object Notation
// JSON is a data format used by Javascript as well as other programming langguage
// JSON objects are NOT to be confused with Javascript Objects
// Used of double quotation marks is required in JSON OBjects

// Syntax :
// {
// 	"propertyA": "valueA",
// 	"propertyB": "valueB",
// }

// {
// 	"city" : "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines",
// }

// Array of JSON Objects
// {
// 	"cities" : [
// 		{"city": "quezon city"},
// 		{"city": "makati city"},
// 	]
// }

// Converting JS Data into Stringified JSON
// - Stringified JSON is a javascript Object converted into a string to be used by the receiving back-end application or other functions of a Javascript application

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'},
]

let stringifiedData = JSON.stringify(batchesArr)
console.log(stringifiedData)

// Converting Stringified JSON into Javascript Objects

let fixedData = JSON.parse(stringifiedData)
console.log(fixedData)